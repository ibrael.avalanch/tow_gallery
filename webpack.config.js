var config = {};

// Detect how npm is run and branch based on that
switch(process.env.npm_lifecycle_event) {
    case 'build':
        config = require('./webpack.config.prod')
        break;
    default:
        config = require('./webpack.config.dev');
        break;
}


module.exports = config