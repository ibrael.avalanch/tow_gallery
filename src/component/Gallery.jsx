import React, {Component} from 'react'
import {connect} from 'react-redux'
import {render, unmountComponentAtNode, findDOMNode} from 'react-dom'
import {play, vote, loadMore, shareFacebook, next, prev} from '../actions/app'
import {Provider} from 'react-redux'
import Waypoint from 'react-waypoint'
/**
 *
 */
@connect(state => ({
    voting: state.voting,
    searching: state.searching,
    meta: state.meta,
    playing: state.playing,
    list: state.list,
    granted: state.granted
}))

/**
 *
 */
export default class Gallery extends Component {

    static contextTypes = {
        store: React.PropTypes.object
    }

    constructor(props, context) {
        super(props, context )
    }

    openPlayer () {
        let {playing, granted, dispatch, list, voting} = this.props,
            {store} = this.context

        if(playing !== null ){
            openPopin(null, {

                className: 'video__popin',
                onLoad: (node)=> {

                    console.log(node)
                    render (
                        <Provider store={store} >
                            <Player  video={list[playing]} voting={voting === playing} index={playing} granted={granted} dispatch={dispatch} />
                        </Provider>,
                        node
                    )

                },
                onClose: (node) => {

                    console.log(node, 'on close')

                    dispatch(play(null)).then( index => {

                        try {
                            unmountComponentAtNode(node);
                            console.log('here')

                        }catch(e) {
                            console.log('here unmount')
                            //node.innerHTML = null;
                        }

                    })

                }

            })
        }
    }

    componentDidUpdate () {
        this.openPlayer()

    }

    render() {

        let {list, dispatch, playing, granted, searching, meta, voting} = this.props,
            className = list.length < 2 ? 'gallery__project gallery__project--full' : 'gallery__project';

        let content = null;

        if(list.length === 0 && !searching && meta.query) {
            content = <div className="gallery__projects--noresults">
                <h3>Oups</h3>
                <p>{Translator.trans('gallery.no_results', {}, 'TOWAppBundle')}</p>
            </div>
        }
        else {
            content = list.map((v, k) =>
                <Project granted={granted}
                         playing={playing === k}
                         className={className}
                         index={k}
                         dispatch={dispatch} key={k} video={v}
                         voting={voting === k }
                />
            )
        }

        return <div className="gallery__projects" ref={ node => this.node = node } >
            {content}

            <Waypoint onEnter={e => dispatch(loadMore())}>
                <div style={{ height: '10px', width: '100%', display: 'block'}}>

                </div>
            </Waypoint>
        </div>

    }
}


class Project extends Component {

    constructor(props) {
        super(props)
        this.play = this.play.bind(this)
        this.close = this.close.bind(this)

    }

    componentDidMount() {

        let {video} = this.props

        var downloadingImage = new Image(),
            image  = document.getElementById(video.id);

        image.style.opacity = 0;

        downloadingImage.onload = function(){
            image.src = this.src;
            image.style.opacity = 1;
        };

        downloadingImage.src = video.poster_url;
    }

    play(index) {

        let { dispatch, meta } = this.props

        dispatch(play(index))
    }


    close() {

        let {dispatch} = this.props

        dispatch(play(null))
    }

    render() {

        let {video, className, index, granted, dispatch, voting} = this.props


        return <div className={className} key="pro" >
            <div itemScope itemType="http://schema.org/VideoObject">
                <div className="gallery__project-name" itemProp="name">
                    <a className="popin__trigger" onClick={e => this.play(index)}>
                        <span>{video.author.fullname}</span>
                        <p className="play">Play</p>
                    </a>
                </div>

                <img id={video.id} className="gallery__project-pic" itemProp="thumbnailUrl" src={video.poster_url} alt={video.title}/>

                <VoteButton voting={voting} video={video} index={index} granted={granted} dispatch={dispatch}/>
                <Social video={video} index={index}  dispatch={dispatch}/>
            </div>
        </div>
    }
}

/**
 *
 */
class VoteButton extends Component {

    constructor(props) {
        super(props)
        this.vote = this.vote.bind(this)
        this.logIn = this.logIn.bind(this)
    }

    logIn () {
        openPopin(
            Routing.generate('register', { 'redirect': Routing.generate('tow_app_gallery')}),
            {
                className: 'gallery__popin',
                onLoad: onLogin,
                onClose: onClose
            }
        )
    }

    vote() {
        let {dispatch, index} = this.props

        dispatch(vote(index)).catch(res => notify(res.message, 'info'))
    }

    render() {

        let {granted, video, voting} = this.props

        if (granted) {


            if (!video.votedByMe) {
                return <a className={ "user-vote gallery__project-vote gallery__project-vote--allowed allowed"}
                          onClick={this.vote}>
                    <span className="video__share-num">{video.votes_count}</span>
                    <i className={"icon icon-coeur-video"   + (voting ? ' loader' : '')}/>
                </a>
            }

            return <a className="user-vote gallery__project-vote gallery__project-vote--voted">
                <span className="video__share-num">{video.votes_count}</span>
                <i className="icon icon-coeur-video-full"/>
            </a>
        }


        return <a className="gallery__project-vote" onClick={this.logIn}>
            <span className="video__share-num">{video.votes_count}</span>
            <i className="icon icon-coeur-video"/>
        </a>
    }
}


class Social extends Component {

    share() {
        let {dispatch, index} = this.props

        dispatch(shareFacebook(index))
    }

    deployOnMobile (e) {
        e.target.classList.toggle('gallery__project-share--active')
    }

    render() {

        let {video} = this.props,
             {facebook_share_count, twitter_share_count, author} = video

        return <div className="gallery__project-share" onClick={this.deployOnMobile.bind(this)}>
            { author.profile_url ?
                <a className="gallery__project-portfolio" target="_blank" href={author.profile_url} /> : null }
            <a key="fb_share" className="gallery__project-fb" onClick={this.share.bind(this)} title={facebook_share_count}>
                <span key="fb_share_count" className="video__share-num"> {facebook_share_count}</span>
            </a>
            <a className="gallery__project-tw" href="#" onClick={tweet} title={twitter_share_count}>
                <span className="video__share-num"> {twitter_share_count}</span>
            </a>
        </div>
    }
}

@connect (({ playing, voting, granted}) => ({ playing, voting, granted}))
class Player extends Component {

    render() {

        let {video, dispatch, index, granted, voting} = this.props

        var params = {
            muted: false,
            quality: 'auto',
            autoplay: true,
            wmode: 'transparent',
            controls: true,
            'endscreen-enable': false,
            related: false,
            'sharing-enable': false
        }, qs = ''

        Object.keys(params).map( p => qs += `${p}=${params[p] ? params[p] : 0}&`)

        return <div className="gallery_player">
                <a className="video__nav video__nav--prev"  title="Voir la vidéo précédente" onClick={e => dispatch(prev())}>
                    <span className="icon" data-icon="v" />
                </a>
                <a className="video__nav video__nav--next"  title="Voir la vidéo précédente" onClick={e => dispatch(next())}>
                    <span className="icon" data-icon="w"/>
                </a>


                <div id="player_container">
                    <iframe id="player" frameBorder="0" width="100%" height="100%" src={`//www.dailymotion.com/embed/video/${video.dm_id}?${qs}`} />
                </div>



                <div className="video__infos">
                    <div className="video__infos-top">
                        <span className="video__author">
                            {video.author.artistic_name ? video.author.artistic_name : video.author.fullname}
                        </span>

                        <div className="video__infos-items">
                            <VoteButton video={video} index={index} dispatch={dispatch} granted={granted} voting={voting}/>
                            <Social video={video} index={index}  dispatch={dispatch}/>
                        </div>
                    </div>
                </div>

                <div className="video__desc">
                    <div className="video__switch" onClick={ (e => document.getElementsByClassName('video__desc')[0].classList.toggle('video__desc--opened'))}>
                        <div className="video__switch-container" >
                            <span className="video__switch-icon"/>
                        </div>
                    </div>
                    <div className="video__desc-inner">
                        <h1 className="video__title">{video.title}</h1>
                        <p className="video__text">{video.description}</p>
                        <p className="video__credits">
                            {video.credits}
                        </p>
                    </div>
                </div>
        </div>

    }
}
