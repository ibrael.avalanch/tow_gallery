/**
 * Created by iespinosa on 29/12/2016.
 */

import { combineReducers } from 'redux';

const granted = ( state = false, action ) => {
    return state
}

const searching = ( state = false, action ) => {
    switch (action.type) {
        case 'REQUEST_START':
            state = true
            break;

        case 'REQUEST_SUCCESS':
            state = false
            break;
    }

    return state
}

const voting = (state = null, action) => {
    switch (action.type) {
        case 'VOTE_START':
            state = action.index
            break;

        case 'VOTE_SUCCESS':
            state = null
            break;
    }

    return state
}

const playing = ( state = null, action ) => {
    switch (action.type) {
        case 'VIDEO_PLAYING':
            state = action.index
            break;
    }

    return state
}

const list = (state = [], action) => {

    let { json } = action

    switch(action.type){

        case 'APPEND_VIDEOS':
            state = state.concat(json.data)
            break;

        case 'LOADED_VIDEOS':
            state = json.data
            break;

        case 'UPDATE_VIDEO':

            let list  = state;
            list[action.index] = action.video

            state = list
            break;
    }

    return state
}

const meta = (state = { offset: 0, limit: 12, has_more: true }, action) => {

    switch(action.type){
        case 'APPEND_VIDEOS':
            state = action.json.meta
            break;

        case 'LOADED_VIDEOS':
            let { json } = action
            state = json.meta
            break;

    }

    return state;
}


const reducers = combineReducers({
    meta,
    list,
    searching,
    playing,
    granted,
    voting
})

export default reducers
