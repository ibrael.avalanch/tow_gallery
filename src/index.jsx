if (!global._babelPolyfill)
    require('babel-polyfill');

import React from 'react';
import { render } from 'react-dom';
import configureStore from './store/configureStore';
import App from './component/App'
import {loadVideos, loadMore} from './actions/app'


const store = configureStore(window.___INITIAL__STATE___);


/**
 *
 * @param element
 * @param options
 */
function renderApp (element, options = {}) {

    //console.log(id,options)

    window.__MODERATION__OPTIONS = options
    
    render(
        <App store={ store } options={options} />,
        element
    );
}



window.gallery = renderApp

store.dispatch(store.getState().list.length > 0 ? loadMore() : loadVideos())